# LAB_GEO_SEIS01

Jupyter Notebook per costruire un grafico distanza-tempo osservato di eventi sismici noti

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Flab_geo_seis01/HEAD?labpath=UNIMIB_LAB_GEO_SEIS01_grafico_distanza_tempo.ipynb)
